public class Ticket {
    // attributes that declared bellow are att that will be shown in the ticket
    // information
    private String nama;
    private String asal;
    private String tujuan;

    // basic setter and getter method
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    // first construction for tambah tiket kereta komuter that contains one
    // parameter which is nama as written in the main
    public Ticket(String nama) {
        setNama(nama);
    }

    // second construction for tambah tiket KAJJ that contains three parameters
    // which is nama, asal, and tujuan as written in the main
    public Ticket(String nama, String asal, String tujuan) {
        setNama(nama);
        setAsal(asal);
        setTujuan(tujuan);
    }

    // a method for collecting every ticket information
    public void ticketInfo() {
        String display = "";
        if (asal == null && tujuan == null) {
            display += "Nama Penumpang: " + this.nama;
        } else {
            display += "Nama Penumpang: " + this.nama + "\nAsal          : " + this.asal + "\nTujuan        : "
                    + this.tujuan;
        }
        System.out.println(display);
        System.out.println("---------------------------------------------------------------------");
    }
}