public class Kereta {
    // att declaration for kereta information esp KAJJ
    private String namaKereta;
    private int sisaTiket;
    private Ticket[] ticket; // att declaration that will be used as tambahTiket container.

    // basic setter and getter method
    public String getNamaKereta() {
        return namaKereta;
    }

    public void setNamaKereta(String namaKereta) {
        this.namaKereta = namaKereta;
    }

    public int getSisaTiket() {
        return sisaTiket;
    }

    public void setSisaTiket(int sisaTiket) {
        this.sisaTiket = sisaTiket;
    }

    public Ticket[] getTicket() {
        return ticket;
    }

    public void setTicket(Ticket[] ticket) {
        this.ticket = ticket;
    }

    // a constructor for object komuter
    public Kereta() {
        setNamaKereta("Komuter");
        setSisaTiket(1000); // based on the system requirement that kereta komuter have 1.000 tickets
                            // available
        ticket = new Ticket[this.sisaTiket];
    }

    // a constructor for object KAJJ
    public Kereta(String namaKereta, int sisaTiket) {
        setNamaKereta(namaKereta);
        setSisaTiket(sisaTiket);
        ticket = new Ticket[this.sisaTiket];
    }

    // a tambahTiket method for booking a kereta komuter ticket
    // this method contains one parameter which is nama
    // the data bellow will be collected in tiket[]
    public void tambahTiket(String nama) {
        for (int i = 0; i < ticket.length; i++) {
            if (ticket[i] != null) { // if ticket[i] is not empty, the program will continue to the next index
            } else {
                this.ticket[i] = new Ticket(nama); // program will fill an empty index. this syntax is linked to first
                                                   // constructor in Ticket class
                break;
            }
        }
        this.sisaTiket--; // sisa tiket will decrese every time the method is called because the ticket is
                          // booked
        outputMsg(); // output message that the purchase is success or not.
    }

    // a tambahTiket method for booking a KAJJ ticket
    // this method contains three parameters which is nama, asal, and tujuan
    // the data bellow will be collected in tiket[]
    public void tambahTiket(String nama, String asal, String tujuan) {
        for (int i = 0; i < ticket.length; i++) {
            if (ticket[i] != null) { // if ticket[i] is not empty, the program will continue to the next index
            } else {
                this.ticket[i] = new Ticket(nama, asal, tujuan); // program will fill an empty index. this syntax is
                                                                 // linked to second constructor in
                                                                 // Ticket class
                break;
            }
        }
        this.sisaTiket--; // sisa tiket will decrese every time the method is called
        outputMsg(); // output message that the purchase is success or not
    }

    // a tampilkanTiket method for displaying booked tickets list
    public void tampilkanTiket() {
        System.out.println("=====================================================================");
        System.out.println("\t\tDaftar Penumpang Kereta Api " + getNamaKereta());
        System.out.println("=====================================================================");
        for (Ticket i : ticket) {
            if (i == null) {
                break;
            } else {
                i.ticketInfo(); // if ticket[i] is empty the the program will move to the next index, and if
                                // ticket[i] is not empty the program will display the ticket info as written in
                                // the ticket class
            }
        }
    }

    public void outputMsg() {
        String display = "";
        if (sisaTiket >= 30) { // if the sisa tiket is up to 30, the purchase is success but the sisa tiket is
                               // not displayed
            display += "Tiket berhasil dipesan.";
        }
        if (sisaTiket < 30 && sisaTiket >= 0) { // then, when the ticket is less than 30, the sisa tiket will be
                                                // displayed
            display += "Tiket berhasil dipesan. Sisa tiket tersedia: " + getSisaTiket();
        }
        if (sisaTiket < 0) { // if the sisa tiket is less than 0, in other word the ticket is sold out, the
                             // purchase is failed.
            display = "Maaf tiket sudah habis dipesan, silakan cari keberangkatan lain.";
        }
        System.out.println("---------------------------------------------------------------------");
        System.out.println(display);
        System.out.println("---------------------------------------------------------------------");
    }

}
