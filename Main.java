public class Main {
    // Manipulasi kode pada kelas Ticket dan Kereta agar kode program di main class
    // dapat berjalan
    public static void main(String[] args) {
        // Jangan ubah kedua objek dibawah ini! menambahkan objek diperbolehkan.
        Kereta komuter = new Kereta();
        komuter.tambahTiket("Ahmad Python");
        komuter.tambahTiket("Junaedi Kotlin");
        komuter.tambahTiket("Saiful HTML");
        komuter.tampilkanTiket();

        // KAJJ memiliki parameter nama kereta dan jumlah tiket tersedia.
        Kereta KAJJ = new Kereta("Jayabaya", 2);
        KAJJ.tambahTiket("Vania Malinda", "Malang", "Surabaya Gubeng");
        KAJJ.tambahTiket("Sekar SD", "Malang", "Sidoarjo");
        KAJJ.tambahTiket("Bonaventura", "Malang", "Surabaya Pasarturi");
        KAJJ.tampilkanTiket();

        Kereta KAJJ2 = new Kereta("Sumeru", 32);
        KAJJ2.tambahTiket("Vania Malinda", "Malang", "Surabaya Gubeng");
        KAJJ2.tambahTiket("Sekar SD", "Malang", "Sidoarjo");
        KAJJ2.tambahTiket("Bonaventura", "Malang", "Surabaya Pasarturi");
        KAJJ2.tampilkanTiket();  
                                // 
        //Nama method tambahTiket dan tampilkanTiket tidak perlu diubah, sesuaikan pada kelas Ticket dan Kereta!
    }
}
